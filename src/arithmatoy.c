#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  size_t result_index = max_len + 1;
  char *result = calloc(max_len + 2, sizeof(char));

  unsigned int rest = 0;

  while (len_lhs > 0 || len_rhs > 0 || rest != 0) {
    unsigned int lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    unsigned int rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);
    unsigned int sum = lhs_digit + rhs_digit + rest;
    rest = sum / base;
    unsigned int digit = sum % base;

    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %u\n", to_digit(digit), rest);
    }

    result[--result_index] = to_digit(digit);
  }

  if (VERBOSE && rest != 0) {
    fprintf(stderr, "add: final carry %u\n", rest);
  }

  return result + result_index;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  if (strcmp(lhs, rhs) == 0) {
    return strdup("0");
  } else if (strcmp(rhs, "0") == 0) {
    return strdup(lhs);
  }

  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  if (len_lhs < len_rhs || (len_lhs == len_rhs && strcmp(lhs, rhs) < 0)) {
    return NULL;
  }

  size_t max_len = len_lhs;
  char *result = calloc(max_len + 1, sizeof(char));
  int rest = 0, result_index = max_len;

  while (len_lhs > 0 || len_rhs > 0) {
    unsigned int lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    unsigned int rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);
    int difference = lhs_digit - rhs_digit - rest + base;
    rest = (difference >= base) ? 0 : 1;
    unsigned int digit = difference % base;

    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %c carry %u\n", to_digit(digit), rest);
    }

    result[--result_index] = to_digit(digit);
  }

  while (result[result_index] == '0') {
    ++result_index;
  }

  return strdup(result + result_index);
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  if (strcmp(lhs, "0") == 0 || strcmp(rhs, "0") == 0) {
    return strdup("0");
  }

  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  size_t max_len = len_lhs + len_rhs;
  unsigned int *result = calloc(max_len, sizeof(unsigned int));

  for (size_t i = 0; i < len_lhs; i++) {
    unsigned int carry = 0;
    unsigned int digit_lhs = get_digit_value(lhs[len_lhs - 1 - i]);
    for (size_t j = 0; j < len_rhs; j++) {
      unsigned int digit_rhs = get_digit_value(rhs[len_rhs - 1 - j]);
      unsigned int sum = result[i + j] + digit_lhs * digit_rhs + carry;
      result[i + j] = sum % base;
      carry = sum / base;
    }
    if (carry > 0) {
      result[i + len_rhs] += carry;
    }
  }

  while (max_len > 1 && result[max_len - 1] == 0) {
    max_len--;
  }

  char *result_str = calloc(max_len + 1, sizeof(char));
  for (size_t i = 0; i < max_len; i++) {
    result_str[max_len - 1 - i] = to_digit(result[i]);
  }

  free(result);
  return result_str;
}

unsigned int get_digit_value(char digit) {
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  size_t length = strlen(str);
  for (size_t i = 0; i < length / 2; ++i) {
    char tmp = str[i];
    str[i] = str[length - 1 - i];
    str[length - 1 - i] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  while (*number == '0') {
    ++number;
  }
  return (*number == '\0') ? number - 1 : number;
}

void debug_abort(const char *debug_msg) {
  fprintf(stderr, "%s\n", debug_msg);
  exit(EXIT_FAILURE);
}
