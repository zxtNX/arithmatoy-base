def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"
    else:
        return ("S" * n) + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == "0":
        return a
    elif b == "0":
        return b
    else:
        return nombre_entier(len(a.strip("0")) * len(b.strip("0")))


def facto_ite(n: int) -> int:
    factorial = 1
    for i in range(1, n + 1):
        factorial *= i
    return factorial


def facto_rec(n: int) -> int:
    if n == 1 or n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for i in range(2, n + 1):
            a, b = b, a + b
        return b


def golden_phi(n: int) -> int:
    return fibo_ite(n + 1) / fibo_ite(n)


def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1


def pow(a: float, n: int) -> float:
    return a**n
